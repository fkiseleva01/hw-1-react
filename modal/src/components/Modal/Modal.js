import React, {Component} from "react";
import "./modal.css";
import Button from "../Button/Button";
import PropTypes  from "prop-types";


class Modal extends Component {

    render() {
      const {title, text, state, onClick} = this.props

      return(    
      <> 
      {state && <div className="modal__overlay" state={state} onClick={onClick} >
        <div className="model__win" onClick={e => e.stopPropagation()}>
            <div className="modal__header">
                <h2 className="modal__title">{title}</h2>
                <Button className="modal__close-btn" onClick={onClick} backgroundColor="rgb(189, 65, 34)" text="&#10006;"/>
            </div>
            <div className="modal__body">
                <p className="modal__text">{text}</p>
                <div className="modal__btn-container">
                    <Button 
                    className="modal__btn"  
                    text="Ok"
                    onClick={()=>alert("Ok!")}
                    />
                    <Button 
                    className="modal__btn" 
                    text="Cansel"
                    onClick={onClick}
                    />
                </div>
            </div>
          </div>
      </div>
      }
      </>
      )
      
    }
  }


  // Modal.PropTypes = {
  //   title: PropTypes.string.isRequired,
  //   text: PropTypes.string.isRequired,
  //   state:PropTypes.bool, 
  //   onClick: PropTypes.func
  // }
  
export default Modal;
  
