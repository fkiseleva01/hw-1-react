import React, {Component} from "react";
import "./button.css";

class Button extends Component {

    render () {
      const { className, onClick, text, backgroundColor } = this.props
      return (
          <div>
            <button className={className}
            style={{backgroundColor: backgroundColor}}
            onClick={onClick}>
            {text}
            </button>
          </div>
      );
    }
  }
  
  // Button.PropTypes = {
  //   title: PropTypes.string.isRequired,
  //   text: PropTypes.string.isRequired,
  //   backgroundColor: PropTypes.object, 
  //   onClick: PropTypes.func.isRequired
  // } 

export default Button;