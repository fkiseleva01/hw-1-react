import {React, Component} from "react";
import app from "./app.css";
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";



class App extends Component{

  constructor () {
    super();
    this.state = {
      showModal1: false,
      showModal2: false,
    }
  }


handleFirstModalOpen() {
  this.setState((state) =>({...state, showModal1: !state.showModal1}))
}
 
handleSecondModalOpen() {
  this.setState((state) =>({...state, showModal2: !state.showModal2}))
}
  render() {
   
    return(
    <div className={app}>
      <div className="btn__btn-wraper">
        <Button 
        className="btn__btn"
        text={"Open first modal"}
        onClick={this.handleFirstModalOpen.bind(this)}
        backgroundColor="rgb(173, 45, 13)"
        />
        <Button 
        className="btn__btn"
        backgroundColor="rgb(189, 65, 34)"
        text={"Open second modal"}
        onClick={this.handleSecondModalOpen.bind(this)}
        />
        </div>
        <Modal
        state={this.state.showModal1}
        onClick={()=> this.handleFirstModalOpen()}
        title={"important news"}
        text={"There are some very important news here, don't you agree?"}
        />
        <Modal
        state={this.state.showModal2}
        onClick={()=> this.handleSecondModalOpen()}
        title={"not important news"}
        text={"There are not news here, are you agree?"}
        />
    

    
      

    </div>
    )  
  }
}

export default App;
